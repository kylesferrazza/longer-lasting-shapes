# Sound changes that lead to seeing longer-lasting shapes
[Research Paper published to Attention, Perception, & Psychophysics][paper]

[Working Program][program]

This project was used for a study on the effect of audio on visual perception.

It was included in a research paper, entitled "Sound changes that lead to seeing longer-lasting shapes", created by Dr. Arthur Samuel and Kavya Tangella.

The research paper became a scholar paper (semifinalist) for the [Regeneron Science Talent Search][regeneron].

[paper]: https://link.springer.com/article/10.3758/s13414-017-1475-6
[program]: https://shapes.kylesferrazza.com
[regeneron]: https://student.societyforscience.org/regeneron-sts

# Measurements
| Name | Distance |
|----|----|
| Blue circle radius | 7.5px |
| Small gray circle radius | 6px |
| Big gray circle radius | 22px |
| Blue center to gray center | 200px |
